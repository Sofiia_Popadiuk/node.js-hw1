const fs = require('fs');
const path = require('path');
const express = require('express')
const morgan = require('morgan')
const app = express()
const PORT = 8080;
const fileTypes = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']
const message200 = 'Success'
const message500 = "Server error"

app.get('/', (req, res) => {
    res.send('Hello in my server!')
})

app.listen(PORT, () => {
    console.log('Server START...')
})

app.use(express.json())
app.use(morgan('\n:method :url :status :res[content-length] - :response-time ms'));
app.use(express.urlencoded({ extended: false }));

fs.stat(path.join(__dirname, 'files'), error => {
    if (error) {
        fs.mkdir(path.join(__dirname, 'files'), error => {
            if (error) {
                console.error(error)
                res.status(500).send({ message: message500 })
            }
            console.log('Folder "files" created');
        })
    }
})

app.post('/api/files', (req, res) => {
    let fileName = req.body.filename;
    if (fileName) {
        if (fileTypes.includes(path.extname(fileName))) {
            fs.stat(path.join(__dirname, 'files', fileName), error => {
                if (!error) {
                    console.log(`File '${fileName}' already exist`)
                    res.status(400).send({ message: "Please specify 'content' parameter" })
                } else {
                    fs.writeFile(path.join(__dirname, 'files', fileName), req.body.content, error => {
                        if (error) {
                            console.error(error)
                            res.status(500).send({ message: message500 })
                        }
                        console.log(`New file '${fileName}' created`)
                        res.status(200).send({ message: 'File created successfully' })
                    })
                }
            })
        } else {
            console.log(`${path.extname(fileName)} is not allowed`)
            res.status(400).send({ message: "Bad file extention" })
        }
    } else {
        res.status(400).send({ message: "No file name" })
    }

})

app.get('/api/files/:filename', (req, res) => {
    let fileName = req.params.filename

    fs.stat(path.join(__dirname, 'files', fileName), error => {
        if (!error) {
            fs.readFile(path.join(__dirname, 'files', fileName), 'utf-8', (error, content) => {
                if (error) {
                    console.log(error);
                    res.status(500).send({ message: message500 })
                }
                console.log(`Content from '${fileName}': ${content}`);
                res.status(200).send({ message: message200, filename: fileName, content: content, extension: path.extname(fileName).substring(1), uploadedDate: new Date() })
            })
        } else {
            console.log(`File '${fileName}' is not exist`)
            res.status(400).send({ message: `No file with '${fileName}' filename found'` })
        }
    })
});

app.get('/api/files', (req, res) => {
    let arr = []
    fs.readdir(path.join(__dirname, 'files'), (error, files) => {
        if (error) {
            console.error(error)
            res.status(500).send({ message: message500 })
        }
        else {
            if (!!files.length) {
                console.log(`\nFiles from ${path.join(__dirname, 'files')}:`);
                files.forEach(file => {
                    arr.push(file)
                    console.log(`${arr.length}. ${file}`)
                })
                res.status(200).send({ message: message200, files: arr })
            }
            else {
                console.log(`\nNo files in ${path.join(__dirname, 'files')}`);
                res.status(200).send({ message: message200, files: arr })
            }
        }
    })
});

app.delete('/api/files/:filename', (req, res) => {
    let fileName = req.params.filename

    fs.stat(path.join(__dirname, 'files', fileName), error => {
        if (!error) {
            fs.unlink(path.join(__dirname, 'files', fileName), error => {
                if (error) {
                    console.error(error)
                    res.status(500).send({ message: message500 })
                }
                console.log(`File '${fileName}' deleted`)
            })
            res.status(200).send({ message: 'File deleted successfully', filename: fileName })
        } else {
            console.log(`File '${fileName}' is not exist`)
            res.status(400).send({ message: `No file with '${fileName}' filename found` })
        }
    })
})

app.put('/api/files', (req, res) => {
    let fileName = req.body.filename;
    fs.stat(path.join(__dirname, 'files', fileName), error => {
        if (error) {
            console.log(`File '${fileName}' is not exist`)
            res.status(400).send({ message: `No file with '${fileName}' filename found'` })
        } else {
            fs.writeFile(path.join(__dirname, 'files', fileName), req.body.content, error => {
                if (error) {
                    console.error(error)
                    res.status(500).send({ message: message500 })
                }
                console.log(`File '${fileName}' modified`)
                res.status(200).send({ message: 'File modify successfully' })
            })
        }
    })
})